
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author computer
 */
public class testsecpro {

    public static void main(String[] args) {
        Connection con = null;
        String dbPath = "/.DB/product";

        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.println("Data Connection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Not Exist");
        } catch (SQLException ex) {
            System.out.println("Cant' Connected");
        }
//process
        try {
            String sql ="SELECT id,name,price FROM product";
            Statement stm = con.createStatement();
            ResultSet result = stm.executeQuery(sql);
            while(result.next()){
               int id = result.getInt("id");
               String name = result.getString("name");
               double price = result.getDouble("price");
                System.out.println(id+" "+name+" "+price);
            }
        } catch (SQLException ex) {
            Logger.getLogger(testsecpro.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            System.out.println("Can't Close");
        }

    }
}
