
import java.sql.Connection;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author computer
 */
public class Testconnection {

    public static void main(String[] args) {
        Connection con = null;
        String dbPath = "/.DB/product";

        try {
            Class.forName("org.sqlite.JDBC");
            con = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
            System.out.println("Data Connection");
        } catch (ClassNotFoundException ex) {
            System.out.println("Not Exist");
        } catch (SQLException ex) {
            System.out.println("Cant' Connected");
        }
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            System.out.println("Can't Close");
        }

    }
}
